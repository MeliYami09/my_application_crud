package com.example.myapplicationcrud;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.Toast;
import android.widget.EditText;
import android.widget.ListView;

import com.example.myapplicationcrud.model.camisas;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    private List<camisas> Listcamisas = new ArrayList<camisas>();
    ArrayAdapter<camisas> ArrayAdapter;
    EditText coL, talL, canT, maR;
    ListView lisV_camisas;
    FirebaseDatabase firebaseDatabase;
    com.google.firebase.database.DatabaseReference DatabaseReference;

    camisas seleccioncamisas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        coL= findViewById(R.id.edit_Color);
        talL = findViewById(R.id.edit_Talla);
        canT= findViewById(R.id.edit_Cantidad);
        maR= findViewById(R.id.edit_Marca);

        lisV_camisas= findViewById(R.id.lv_datosCamisas);
        inicialFirebase();
        listaDatos();


        lisV_camisas.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                seleccioncamisas=(camisas) parent.getItemAtPosition(position);
                coL.setText(seleccioncamisas.getColor());
                talL.setText(seleccioncamisas.getTalla());
                canT.setText(seleccioncamisas.getCantidad());
                maR.setText(seleccioncamisas.getMarca());
            }
        });



    }

    private void listaDatos() {
        DatabaseReference.child("camisas").addValueEventListener(new ValueEventListener() {
            private ArrayAdapter<camisas> ArrayAdapte;

            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Listcamisas.clear();
                for(DataSnapshot objSnaptshot: snapshot.getChildren()){
                    camisas p = objSnaptshot.getValue(camisas.class);
                    Listcamisas.add(p);

                    ArrayAdapte = new ArrayAdapter<camisas>(MainActivity.this, android.R.layout.simple_list_item_1, Listcamisas );
                    ListAdapter arrayAdapter = null;
                    lisV_camisas.setAdapter(arrayAdapter);

                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    private void inicialFirebase() {
        FirebaseApp.initializeApp(this);
        firebaseDatabase =firebaseDatabase.getInstance();
        //  firebaseDatabase.setPersistenceEnabled(true); // persistencia
        DatabaseReference = firebaseDatabase.getReference();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected( MenuItem item) {
        String color= coL.getText().toString();
        String talla = talL.getText().toString();
        String cantidad = canT.getText().toString();
        String marca = maR.getText().toString();
        switch (item.getItemId()){
            case R.id.icon_add: {
                if(color.equals("")||talla.equals("")||cantidad.equals("")||marca.equals("")){
                    validacion();
                }else {
                    camisas p= new camisas();
                    p.setId(UUID.randomUUID().toString());
                    p.setColor(color);
                    p.setTalla(talla);
                    p.setCantidad(cantidad);
                    p.setMarca(marca);
                    DatabaseReference.child("camisas").child(p.getId()).setValue(p);
                    Toast.makeText(this, "agregado", Toast.LENGTH_SHORT).show();
                    limpiar();


                }
                break;
            }
            case R.id.icon_save: {
                camisas p = new camisas();
                p.setId(seleccioncamisas.getId());
                p.setColor(coL.getText().toString().trim());
                p.setTalla(talL.getText().toString().trim());
                p.setCantidad(canT.getText().toString().trim());
                p.setMarca(maR.getText().toString().trim());
                DatabaseReference.child("Persona").child(p.getId()).setValue(p);
                Toast.makeText(this, "guardado", Toast.LENGTH_SHORT).show();
                limpiar();
                break;
            }
            case R.id.icon_delete: {
                camisas p = new camisas();
                p.setId(seleccioncamisas.getId());
                DatabaseReference.child("camisas").child(p.getId()).removeValue();


                Toast.makeText(this, "eliminado", Toast.LENGTH_SHORT).show();
                break;
            }
            default:break;
        }
        return true;
    }
        private void limpiar() {
            coL.setText("");
            talL.setText("");
            canT.setText("");
            maR.setText("");
        }
    private void validacion() {
        String color = coL.getText().toString() ;
        String talla = talL.getText().toString();
        String cantidad = canT.getText().toString();
        String marca = maR.getText().toString();
        if (color.equals("")){
            coL.setError("Required");
        }else if(talla.equals("")){
            talL.setError("Required");
        }else if (cantidad.equals("")){
            canT.setError("Required");
        }else if (marca.equals("")){
            maR.setError("Required");

        }

    }
}



