package com.example.myapplicationcrud.model;

import androidx.annotation.NonNull;

public class camisas {
    private String Id;
    private String Color;
    private String Talla;
    private String Cantidad;
    private String Marca;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getTalla() {
        return Talla;
    }

    public void setTalla(String talla) {
        Talla = talla;
    }

    public String getCantidad() {
        return Cantidad;
    }

    public void setCantidad(String cantidad) {
        Cantidad = cantidad;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String marca) {
        Marca = marca;
    }

    public camisas() {

    }


    @Override
    public String toString() { return Marca; }
}

